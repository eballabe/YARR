image: gitlab-registry.cern.ch/yarr/yarr_container:yarr_build

job_build_and_test:
    stage: build
    script:
      - yum -y install boost-devel
      - source scl_source enable devtoolset-7 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - mkdir  build
      - cd build
      - CXXFLAGS=-Werror cmake3 .. -DBUILD_TESTS=on -DCMAKE_TOOLCHAIN_FILE=../cmake/linux-gcc
      - make install -j4
      - export USER=test
      - echo $USER
      - /bin/pwd
      - mkdir /tmp/test
      - ls /tmp/
      - cd ../
      - bin/scanConsole -r configs/controller/emuCfg.json -c configs/connectivity/example_fei4b_setup.json -s configs/scans/fei4/std_digitalscan.json -p -l configs/logging/trace_all.json
      - ls
      - ls ./data/
      - ls ./data/000001_std_digitalscan/
      - cat ./data/000001_std_digitalscan/*_OccupancyMap.json
      - NUM100=`cat ./data/000001_std_digitalscan/*_OccupancyMap.json | grep -o 100 | wc -l`
      - if [ "$NUM100" != "26880" ]; then exit 2; fi
      - bin/scanConsole -k
      - bin/test_main
      - bin/test_star -r 0 configs/controller/emuCfg_star.json

job_build_and_test_out_of_source:
    stage: build
    script:
      - yum -y install boost-devel
      - source scl_source enable devtoolset-7 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - CXXFLAGS=-Werror cmake3 -S . -B /tmp/build -DBUILD_TESTS=on -DCMAKE_TOOLCHAIN_FILE=cmake/linux-gcc
      - cmake3 --build /tmp/build -j4
      - cmake3 --install /tmp/build
      - export USER=test
      - echo $USER
      - /bin/pwd
      - mkdir /tmp/test
      - ls /tmp/
      - /tmp/build/bin/scanConsole -r configs/controller/emuCfg.json -c configs/connectivity/example_fei4b_setup.json -s configs/scans/fei4/std_digitalscan.json -p -l configs/logging/trace_all.json
      - ls
      - ls ./data/
      - ls ./data/000001_std_digitalscan/
      - cat ./data/000001_std_digitalscan/*_OccupancyMap.json
      - NUM100=`cat ./data/000001_std_digitalscan/*_OccupancyMap.json | grep -o 100 | wc -l`
      - if [ "$NUM100" != "26880" ]; then exit 2; fi
      - /tmp/build/bin/scanConsole -k
      - /tmp/build/bin/test_main

job_build_all:
    stage: build
    artifacts:
        paths:
          - bin
          - build
          - lib
    script:
      - yum -y install boost-devel
      - source scl_source enable devtoolset-7 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - mkdir  build
      - cd build
      - cmake3 .. -DBUILD_TESTS=on -DCMAKE_TOOLCHAIN_FILE=../cmake/linux-gcc -DYARR_CONTROLLERS_TO_BUILD=all -DYARR_FRONT_ENDS_TO_BUILD=all -DYARR_ACTIVE_LOGGER_LEVEL=TRACE
      - make install -j4
      - bin/scanConsole -k

job_build_clang:
    stage: build
    script:
      - yum -y install boost-devel
      - source scl_source enable devtoolset-7 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - source scl_source enable llvm-toolset-6.0 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - mkdir -p build
      - cd build 
      - CXXFLAGS=-Werror cmake3 .. -DCMAKE_TOOLCHAIN_FILE=../cmake/linux-clang
      - make install -j4
      - cd ..
      - bin/scanConsole -k

job_test_json_parsing:
    stage: test
    needs: ["job_build_all"]
    script:
      # Check schema by loading configs in turn
      - for file in $(find configs/defaults -iname "*.json"); do echo ${file}; bin/test_json -f ${file} -t FRONT_END || break; done
      - for file in $(find configs/connectivity -iname "*.json"); do echo ${file}; bin/test_json -f ${file} -t CONNECTIVITY || break; done
      - for file in $(find configs/controller -iname "*.json"); do echo ${file}; bin/test_json -f ${file} -t CONTROLLER || break; done
      - for file in $(find configs/scans/* -iname "*.json"); do echo ${file}; bin/test_json -f ${file} -t SCAN_CONFIG || break; done

      - cd build
      - make test

job_test_json:
    stage: build
    script:
      - npm install jsonlint -g
      - for file in $(find ../configs/ -iname "*.json"); do echo ${file}; jsonlint -q ${file} || break -1; done

job_test_code_coverage:
    stage: build
    artifacts:
        paths:
          - cc_tests
    script:
      - source scl_source enable devtoolset-7 || echo ignoring exit code so CI does not bomb when it should not > /dev/null
      - mkdir  build
      - cd build
      - cmake3 .. -DBUILD_TESTS=on -DCMAKE_BUILD_TYPE=Coverage -DCMAKE_TOOLCHAIN_FILE=../cmake/linux-gcc
      - make install -j4
      - export USER=test
      - echo $USER
      - /bin/pwd
      - mkdir /tmp/test
      - ls /tmp/
      - cd ../
      - scripts/cc_tool.sh

deploy_coverage:
    stage: deploy
    dependencies: ["job_test_code_coverage"]
    only:
        - devel@YARR/YARR
    variables:
        "EOS_PATH" : "/eos/user/t/theim/www/yarr/devel/coverage"
        "CI_OUTPUT_DIR" : "cc_tests/"
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer
    script:
        - deploy-eos

build_docs:
    stage: build
    before_script:
        - pip install mkdocs
    script:
        - export LC_ALL=en_US.utf-8
        - export LANG=en_US.utf-8
        - mkdocs build
    artifacts:
        paths:
            - site
          

deploy_master:
    stage: deploy
    dependencies: ["build_docs"]
    only:
        - master@YARR/YARR
    variables:
        "EOS_PATH" : "/eos/user/t/theim/www/yarr"
        "CI_OUTPUT_DIR" : "site/"
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer
    script:
        - deploy-eos

deploy_devel:
    stage: deploy
    dependencies: ["build_docs"]
    only:
        - devel@YARR/YARR
    variables:
        "EOS_PATH" : "/eos/user/t/theim/www/yarr/devel"
        "CI_OUTPUT_DIR" : "site/"
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer
    script:
        - deploy-eos
